Script.Load("lua/NS2Gamerules.lua")

if (Server) then
   function NS2Gamerules:UpdateAutoTeamBalance(dt)
      local team1Players = self.team1:GetNumPlayers()

      self:SetMaxBots(math.max(2, math.ceil(team1Players * kAIA_botsPerMarine)))
   end

   -- Only allow players to join the marine and random team
   function NS2Gamerules:GetCanJoinTeamNumber(teamNumber)
      return teamNumber ~= kAlienTeamType
   end

   local NS2GamerulesOnCreate = NS2Gamerules.OnCreate
   function NS2Gamerules:OnCreate()
      NS2GamerulesOnCreate(self)
      -- Entity.AddTimedCallback(self, _adjustBotsNumber, 1)
   end
end
